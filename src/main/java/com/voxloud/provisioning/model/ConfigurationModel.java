package com.voxloud.provisioning.model;

import java.util.List;

import lombok.Data;

@lombok.Generated
public @Data class ConfigurationModel {
	
	@lombok.Generated
	private String userName;
	
	@lombok.Generated
	private String password;
	
	@lombok.Generated
	private String domain;
	
	@lombok.Generated
	private String port;
	
	@lombok.Generated
	private String codecs;
	
	@lombok.Generated
	private List<String> jsonCodecs;
	
	@lombok.Generated
	private String timeout;
	
	@lombok.Generated
	private String deviceModel;
	
	@lombok.Generated
	private Boolean overrideFragment;
	
	@lombok.Generated
	private String fileType;
	
	@lombok.Generated
	private String macAddress;
	
}
