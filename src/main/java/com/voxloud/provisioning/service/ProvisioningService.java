package com.voxloud.provisioning.service;

import com.voxloud.provisioning.model.ConfigurationModel;

public interface ProvisioningService {

	ConfigurationModel getProvisioningFile(String macAddress);
}
