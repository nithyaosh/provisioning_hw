package com.voxloud.provisioning.service;

import java.io.FileWriter;
import java.io.IOException;
import java.io.StringReader;
import java.util.Arrays;
import java.util.Properties;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.voxloud.provisioning.config.ProvisioningConfig;
import com.voxloud.provisioning.config.ProvisioningConstants;
import com.voxloud.provisioning.entity.Device;
import com.voxloud.provisioning.exception.ProvisoningNoDataException;
import com.voxloud.provisioning.model.ConfigurationModel;
import com.voxloud.provisioning.repository.DeviceRepository;
@Service
public class ProvisioningServiceImpl implements ProvisioningService {
	
	@Autowired 
	private DeviceRepository deviceRepo;
	
	@Autowired 
	private ProvisioningConfig provisioningConfig;
	
	private void buildDeviceOverrideFragment(ConfigurationModel configurationModel, Device device) {
		if(device.getOverrideFragment() != null) {
			if(device.getModel().toString().equalsIgnoreCase(ProvisioningConstants.DESK)) {
				final Properties deskProps = new Properties();
				try {
					deskProps.load(new StringReader(device.getOverrideFragment()));
					configurationModel.setTimeout(deskProps.getProperty(ConfigAttributes.TIMEOUT.toString()));
					configurationModel.setDomain(deskProps.getProperty(ConfigAttributes.DOMAIN.toString()));
					configurationModel.setPort(deskProps.getProperty(ConfigAttributes.PORT.toString()));
				} catch (IOException e) {
					e.printStackTrace();
				}
			} else if(device.getModel().toString().equalsIgnoreCase(ProvisioningConstants.CONFERENCE)) {
				JSONParser parser = new JSONParser(); 
				JSONObject json;
				try {
					json = (JSONObject) parser.parse(device.getOverrideFragment());
					configurationModel.setTimeout(json.get(ConfigAttributes.TIMEOUT.toString()).toString());
					configurationModel.setDomain(json.get(ConfigAttributes.DOMAIN.toString()).toString());
					configurationModel.setPort(json.get(ConfigAttributes.PORT.toString()).toString());
				} catch (ParseException e) {
					e.printStackTrace();
				} 
			}
				
		}
	}
	
	private void buildPropertyFile(ConfigurationModel configurationModel)  {
		final Properties deskProps=new Properties();  
		deskProps.setProperty(ConfigAttributes.USERNAME.toString(),configurationModel.getUserName());  
		deskProps.setProperty(ConfigAttributes.PASSWORD.toString(),configurationModel.getPassword());
		deskProps.setProperty(ConfigAttributes.DOMAIN.toString(),configurationModel.getDomain());  
		deskProps.setProperty(ConfigAttributes.CODECS.toString(), configurationModel.getCodecs());
		if(configurationModel.getOverrideFragment() != null) {
			deskProps.put(ConfigAttributes.TIMEOUT.toString(), configurationModel.getTimeout());
		}
		  
		try {
			deskProps.store(new FileWriter(ProvisioningConstants.DESK+configurationModel.getMacAddress()+".properties"),"DESK CONFIG FILE");
		} catch (IOException e) {
			e.printStackTrace();
		}  
	}
	
	private void buildJSONFile(ConfigurationModel configurationModel)  {
		JSONObject jsonObject = new JSONObject();
		jsonObject.put(ConfigAttributes.USERNAME.toString(),configurationModel.getUserName());  
		jsonObject.put(ConfigAttributes.PASSWORD.toString(),configurationModel.getPassword());
		jsonObject.put(ConfigAttributes.DOMAIN.toString(),configurationModel.getDomain());  
		jsonObject.put(ConfigAttributes.CODECS.toString(), configurationModel.getJsonCodecs());
		if(configurationModel.getOverrideFragment() != null) {
			jsonObject.put(ConfigAttributes.TIMEOUT.toString(), configurationModel.getTimeout());
		}
		  
		try {
			FileWriter file1 = new FileWriter(ProvisioningConstants.CONFERENCE+configurationModel.getMacAddress()+".json");
			file1.write(jsonObject.toJSONString());
			file1.close();
		} catch (IOException e) {
			e.printStackTrace();
		}  
	}

	public ConfigurationModel getProvisioningFile(String macAddress)  {
		Device device = deviceRepo.findByMacAddress(macAddress);
		ConfigurationModel configurationModel  = null;
		if(device != null) {
			configurationModel = new ConfigurationModel();
			configurationModel.setMacAddress(macAddress);
			configurationModel.setDeviceModel(device.getModel().toString());
			configurationModel.setUserName(device.getUsername());
			configurationModel.setPassword(device.getPassword());
			
			if(device.getOverrideFragment() != null) {
				buildDeviceOverrideFragment(configurationModel, device);
				configurationModel.setOverrideFragment(true);
			} else {
				configurationModel.setDomain(provisioningConfig.getDomain());
				configurationModel.setPort(provisioningConfig.getPort());
			}
			if(device.getModel().toString().equalsIgnoreCase(ProvisioningConstants.DESK)) {
				configurationModel.setFileType(FileType.PROPERTY.toString());
				configurationModel.setCodecs(provisioningConfig.getCodeCs());
				buildPropertyFile(configurationModel);
			} else if(device.getModel().toString().equalsIgnoreCase(ProvisioningConstants.CONFERENCE)) {
				String[] splitCodes = provisioningConfig.getCodeCs().split(",");
					configurationModel.setJsonCodecs(Arrays.asList(splitCodes));
				configurationModel.setFileType(FileType.JSON.toString());
				buildJSONFile(configurationModel);
			}
			/*if(overrideFragment) {
				buildDeviceOverrideFragment(configurationModel, device);
			}*/
		} else {
			throw new ProvisoningNoDataException("No IP EXISITS");
		}
		return configurationModel;
	}
    
    public enum FileType {
        PROPERTY,
        JSON
    }
    
    public enum ConfigAttributes {
        USERNAME,
        PASSWORD,
        DOMAIN,
        PORT,
        CODECS,
        TIMEOUT;
        
        @Override
        public String toString() {
         return super.toString().toLowerCase();
        }
    }
}
