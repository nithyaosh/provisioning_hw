package com.voxloud.provisioning.exception;

public class ProvisoningNoDataException extends RuntimeException{

	private static final long serialVersionUID = 1131602127777906563L;

	public ProvisoningNoDataException() {
        super();
    }
	
	public ProvisoningNoDataException(String message) {
        super(message);
    }
}
