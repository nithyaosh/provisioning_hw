package com.voxloud.provisioning.exception;

import java.io.IOException;
import java.sql.SQLException;
import java.text.ParseException;

import org.hibernate.exception.SQLGrammarException;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.orm.jpa.JpaSystemException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.voxloud.provisioning.config.ProvisioningConstants;

@ControllerAdvice
public class ProvisioningExceptionController extends ResponseEntityExceptionHandler {
	
	@ExceptionHandler({ SQLException.class, NullPointerException.class, JpaSystemException.class, SQLGrammarException.class, DataIntegrityViolationException.class, IOException.class, ParseException.class})
	public ResponseEntity<Object> handleException(Exception ex) {
		return new ResponseEntity<>(ProvisioningConstants.INTERNAL_SERVER_ERROR_MSG, HttpStatus.INTERNAL_SERVER_ERROR);
	}
	
	@ExceptionHandler(ProvisoningNoDataException.class)
	public ResponseEntity<Object> handleConfigDataValidException(ProvisoningNoDataException ex) {
		return new ResponseEntity<>(ex.getMessage(), HttpStatus.NOT_FOUND);
	}

}
