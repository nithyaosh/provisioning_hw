package com.voxloud.provisioning.config;

public final class ProvisioningConstants {
	private ProvisioningConstants() {
		
	}
	
	public static final String INTERNAL_SERVER_ERROR_MSG = "Please contact support team";
	
	public static final String DESK = "desk";
	
	public static final String CONFERENCE = "conference";

}
