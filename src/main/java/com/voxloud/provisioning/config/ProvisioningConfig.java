package com.voxloud.provisioning.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import lombok.Data;

@ConfigurationProperties(prefix="provisioning")
@Configuration
@lombok.Generated
public @Data class ProvisioningConfig {
	
	@lombok.Generated
	private String domain;
	
	@lombok.Generated
	private String port;
	
	@lombok.Generated
	private String codeCs;

}
