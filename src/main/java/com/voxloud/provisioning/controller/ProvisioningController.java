package com.voxloud.provisioning.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.voxloud.provisioning.model.ConfigurationModel;
import com.voxloud.provisioning.service.ProvisioningService;

@RestController
@RequestMapping("/api/v1")
public class ProvisioningController {
	
	private final ProvisioningService provisioningService;

	public ProvisioningController(ProvisioningService provisioningService) {
		this.provisioningService = provisioningService;
	}

    // TODO Implement controller method
	@GetMapping("/provisioning/{macAddress}")
	public ResponseEntity<Object> getProvisioningFile(@PathVariable("macAddress") String macAddress) {
		HttpStatus httpStatus = HttpStatus.OK;
		ConfigurationModel configuration = provisioningService.getProvisioningFile(macAddress);
		if (configuration == null)
			httpStatus = HttpStatus.NO_CONTENT;
		return new ResponseEntity<>(configuration, httpStatus);
	 }
}